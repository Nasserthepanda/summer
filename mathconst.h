#include <math.h>
#define E 2.71828182845904
#define UPPERLIMIT 10
#define PI 3.14141414141414

int Factorial(int x);
float Sin(float angle);


int Factorial(int x){
    volatile register int p=1;
    for(int i=1;i<=x;i++){
        p=p*i;
    }
    return p;

}
float Sin(float angle){
    volatile register float series_sum=0;
    volatile register float series_value;

    for(int i=0;i<=UPPERLIMIT;i++){
            series_value=((pow(-1,i))/(Factorial(2*i+1)))*pow(angle,2*i+1);
            series_sum=series_sum+series_value;
    }

    return series_sum;
}


